# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool


class Receipt(metaclass=PoolMeta):
    __name__ = 'agro.goods.receipt'

    @classmethod
    def do(cls, records):
        super().do(records)
        cls.link_uls_to_order(records)

    @classmethod
    def link_uls_to_order(cls, records):
        pool = Pool()
        UnitLoad = pool.get('stock.unit_load')
        to_write = []
        for record in records:
            for line in record.lines:
                order = record._must_link_uls_to_order(line)
                if order:
                    to_write.extend([list(line.unit_loads), {
                        'sale_production_order': order.id}])
        if to_write:
            UnitLoad.write(*to_write)

    @classmethod
    def _must_link_uls_to_order(cls, receipt_line):
        return (receipt_line.harvest_line
            and receipt_line.harvest_line.request_line
            and receipt_line.harvest_line.request_line.production_order
            or None)


class ReceiptProduct(metaclass=PoolMeta):
    __name__ = 'agro.goods.receipt'

    @classmethod
    def _must_link_uls_to_order(cls, receipt_line):
        porder = super()._must_link_uls_to_order(receipt_line)
        if porder and porder.product == receipt_line.product:
            return porder


class ReceiptHarvestless(metaclass=PoolMeta):
    __name__ = 'agro.goods.receipt'

    @classmethod
    def _must_link_uls_to_order(cls, receipt_line):
        res = super()._must_link_uls_to_order(receipt_line)
        if res:
            return res
        return (receipt_line.request_line
            and receipt_line.request_line.production_order
            or None)

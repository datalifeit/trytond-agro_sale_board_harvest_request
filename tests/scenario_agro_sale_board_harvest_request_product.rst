================================================
scenario_agro_sale_board_harvest_request_product
================================================

Imports::

    >>> import time
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.agro_plant.tests.tools \
    ...     import create_plant_order
    >>> from trytond.modules.agro_product.tests.tools \
    ...     import create_attribute_combinations
    >>> from decimal import Decimal
    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta

    >>> today = datetime.date.today()

Install agro_sale_board_harvest_request::

    >>> config = activate_modules(['agro_sale_board_harvest_request', 'agro_production_order_product'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Set context::

    >>> Process = Model.get('business.process')
    >>> process, = Process.find([('name', '=', 'Sale board')], limit=1)
    >>> _ = config._context.setdefault('process_id', process.id)


Create Party::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Party')
    >>> party.save()
    >>> producer2 = Party(name='Producer2')
    >>> producer2.save()


Create crop::

    >>> Productuom = Model.get('product.uom')
    >>> Crop = Model.get('agro.crop')
    >>> crop = Crop(name='Crop 1')
    >>> unit, = Productuom.find([('name', '=', 'Unit')])
    >>> crop.product_in_progress_unit = unit
    >>> crop.save()


Create plant order::

    >>> create_plant_order(config=config)
    >>> Batch = Model.get('agro.farm.batch')
    >>> batch, = Batch.find([], limit=1)
    >>> batch.click('grow')


Create attributes and combinations::

    >>> Template = Model.get('product.template')
    >>> create_attribute_combinations(config, crop=crop)
    >>> template, = Template.find([('name', '=', 'Product with attributes')])
    >>> conf = template.confection_attributes[0].origin
    >>> conf_crop = conf.crops.new()
    >>> conf_crop.quantity_per_case = Decimal('12')
    >>> conf_crop.qty_per_case_uom = template.default_uom
    >>> conf.save()


Create production orders::

    >>> Order = Model.get('agro.production.order')
    >>> Location = Model.get('stock.location')
    >>> wh, = Location.find([('type', '=', 'warehouse')])
    >>> order = Order()
    >>> order.category = template.categories[0]
    >>> order.crop_group = crop.group
    >>> order.crop = crop
    >>> order.attribute_category = template.attribute_combinations[0].category
    >>> order.attribute_caliber = template.attribute_combinations[0].caliber
    >>> order.attribute_confection = template.attribute_combinations[0].confection
    >>> order.uom = template.default_uom
    >>> order.ul_quantity = 4.0
    >>> order.warehouse = wh
    >>> order.party = party
    >>> order.arrival_date = today + relativedelta(days=2)
    >>> order.departure_date = today
    >>> order.unit_price = Decimal(1.5)
    >>> order_line = order.lines.new()
    >>> order_line.ul_quantity
    4.0
    >>> order.save()
    >>> order.click('confirm')

Create request::

    >>> Request = Model.get('agro.harvest.request')
    >>> request = Request()
    >>> request.producer = batch.producer
    >>> request_line = request.lines.new()
    >>> request_line.production_order = order
    >>> request_line.party = producer2
    >>> request.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Production order" in "Harvest Request Line" is not valid according to its domain. - 
    >>> request.reload()
    >>> request_line = request.lines.new()
    >>> request_line.production_order = order
    >>> request_line.party == order.party
    True
    >>> request_line.category == order.category
    True
    >>> request_line.crop_group == order.crop_group
    True
    >>> request_line.crop == order.crop
    True
    >>> request_line.attribute_category == order.attribute_category
    True
    >>> request_line.attribute_caliber == order.attribute_caliber
    True
    >>> request_line.attribute_confection == order.attribute_confection
    True
    >>> request_line.uom == order.uom
    True
    >>> request_line.ul_quantity == order.ul_quantity
    True
    >>> request_line.cases_quantity == order.cases_quantity
    True
    >>> request_line.ul_cases_quantity == order.ul_cases_quantity
    True
    >>> request_line.quantity_per_case == order.quantity_per_case
    True
    >>> request_line.quantity_per_ul == order.quantity_per_ul
    True
    >>> request_line.quantity == order.quantity
    True
    >>> request_line.base_product == template
    True
    >>> request_line.product == template.attribute_combinations[0].result_product.products[0]
    True
    >>> request.click('confirm')

Change product in order with harvest request lines::

    >>> order.reload()
    >>> len(order.lines)
    1
    >>> order.lines[0].ul_quantity
    0.0
    >>> len(order.harvest_request_lines)
    1
    >>> order.click('draft')
    >>> Product = Model.get('product.product')
    >>> product, = [p for p in Product.find([('id', '!=', order.product.id)], limit=1)]
    >>> order.product = product
    >>> order.save() # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    trytond.exceptions.UserError: Cannot modify following fields of Order "1" because it has harvest request lines:
      - Product - 

Check do not exceed production order ULs::

    >>> order.reload()
    >>> order.click('confirm')
    >>> request2, = request.duplicate()
    >>> request2.click('confirm')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Too many ULs for order "1" (to produce: 8.0 ordered: 4.0) - 
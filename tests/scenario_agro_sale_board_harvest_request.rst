========================================
Agro Sale Board Harvest Request Scenario
========================================

Imports::

    >>> import time
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.agro_plant.tests.tools \
    ...     import create_plant_order
    >>> from trytond.modules.agro_product.tests.tools \
    ...     import create_attribute_combinations
    >>> from decimal import Decimal
    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta

    >>> today = datetime.date.today()

Install agro_sale_board_harvest_request::

    >>> config = activate_modules('agro_sale_board_harvest_request')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Set context::

    >>> Process = Model.get('business.process')
    >>> process, = Process.find([('name', '=', 'Sale board')], limit=1)
    >>> _ = config._context.setdefault('process_id', process.id)


Create Party::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Party')
    >>> party.save()
    >>> producer2 = Party(name='Producer2')
    >>> producer2.save()


Create crop::

    >>> Productuom = Model.get('product.uom')
    >>> Crop = Model.get('agro.crop')
    >>> crop = Crop(name='Crop 1')
    >>> unit, = Productuom.find([('name', '=', 'Unit')])
    >>> crop.product_in_progress_unit = unit
    >>> crop.save()


Create plant order::

    >>> create_plant_order(config=config)
    >>> Batch = Model.get('agro.farm.batch')
    >>> batch, = Batch.find([], limit=1)
    >>> batch.click('grow')


Create attributes and combinations::

    >>> Template = Model.get('product.template')
    >>> create_attribute_combinations(config, crop=crop)
    >>> template, = Template.find([('name', '=', 'Product with attributes')])
    >>> conf = template.confection_attributes[0].origin
    >>> conf_crop = conf.crops.new()
    >>> conf_crop.quantity_per_case = Decimal('12')
    >>> conf_crop.qty_per_case_uom = template.default_uom
    >>> conf.save()


Create production orders::

    >>> Order = Model.get('agro.production.order')
    >>> Location = Model.get('stock.location')
    >>> wh, = Location.find([('type', '=', 'warehouse')])
    >>> order = Order()
    >>> order.category = template.categories[0]
    >>> order.crop_group = crop.group
    >>> order.crop = crop
    >>> order.attribute_category = template.attribute_combinations[0].category
    >>> order.attribute_caliber = template.attribute_combinations[0].caliber
    >>> order.attribute_confection = template.attribute_combinations[0].confection
    >>> order.uom = template.default_uom
    >>> order.ul_quantity = Decimal(4)
    >>> order.warehouse = wh
    >>> order.party = party
    >>> order.arrival_date = today + relativedelta(days=2)
    >>> order.departure_date = today
    >>> order.unit_price = Decimal(1.5)
    >>> order_line = order.lines.new()
    >>> order_line.ul_quantity
    4.0
    >>> order.save()
    >>> order.click('confirm')

    >>> order2 = Order()
    >>> order2.category = template.categories[0]
    >>> order2.crop_group = crop.group
    >>> order2.crop = crop
    >>> order2.attribute_category = template.attribute_combinations[0].category
    >>> order2.attribute_caliber = template.attribute_combinations[0].caliber
    >>> order2.attribute_confection = template.attribute_combinations[0].confection
    >>> order2.uom = template.default_uom
    >>> order2.ul_quantity = Decimal(3)
    >>> order2.warehouse = wh
    >>> order2.party = party
    >>> order2.arrival_date = today + relativedelta(days=2)
    >>> order2.departure_date = today
    >>> order2.unit_price = Decimal(1.5)
    >>> order2.save()
    >>> order2.click('confirm')

    >>> order3 = Order()
    >>> order3.reference = 'REF-1'
    >>> order3.category = template.categories[0]
    >>> order3.crop_group = crop.group
    >>> order3.crop = crop
    >>> order3.attribute_category = template.attribute_combinations[0].category
    >>> order3.attribute_caliber = template.attribute_combinations[0].caliber
    >>> order3.attribute_confection = template.attribute_combinations[0].confection
    >>> order3.uom = template.default_uom
    >>> order3.ul_quantity = Decimal(4)
    >>> order3.warehouse = wh
    >>> order3.party = party
    >>> order3.arrival_date = today + relativedelta(days=2)
    >>> order3.departure_date = today
    >>> order3.unit_price = Decimal(1.5)
    >>> order3.click('confirm')

Create request::

    >>> Request = Model.get('agro.harvest.request')
    >>> request = Request()
    >>> request.producer = batch.producer
    >>> request_line = request.lines.new()
    >>> request_line.production_order = order
    >>> request_line.party = producer2
    >>> request.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Production order" in "Harvest Request Line" is not valid according to its domain. - 
    >>> request.reload()
    >>> request_line = request.lines.new()
    >>> request_line.production_order = order
    >>> request_line.party == order.party
    True
    >>> request_line.category == order.category
    True
    >>> request_line.crop_group == order.crop_group
    True
    >>> request_line.crop == order.crop
    True
    >>> request_line.attribute_category == order.attribute_category
    True
    >>> request_line.attribute_caliber == order.attribute_caliber
    True
    >>> request_line.attribute_confection == order.attribute_confection
    True
    >>> request_line.uom == order.uom
    True
    >>> request_line.ul_quantity == order.ul_quantity
    True
    >>> request_line.cases_quantity == order.cases_quantity
    True
    >>> request_line.ul_cases_quantity == order.ul_cases_quantity
    True
    >>> request_line.quantity_per_case == order.quantity_per_case
    True
    >>> request_line.quantity_per_ul == order.quantity_per_ul
    True
    >>> request_line.quantity == order.quantity
    True
    >>> request_line.base_product == template
    True
    >>> request_line.product == template.attribute_combinations[0].result_product.products[0]
    True


Check exceed ul quantity::

    >>> request_line.ul_quantity += 1
    >>> request.click('confirm') # doctest: +ELLIPSIS
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Too many ULs for order "..." (to produce: ... ordered: ...) - 
    >>> request_line, = request.lines
    >>> request_line.ul_quantity = 3
    >>> request.save()

    >>> request_line2 = request.lines.new()
    >>> request_line2.production_order = order
    >>> request.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelsql.SQLConstraintError: Production order must be unique per Harvest request. - 
    >>> _ = request.lines.pop(request.lines.index(request_line2))
    >>> request_line2 = request.lines.new()
    >>> request_line2.production_order = order2
    >>> request.click('confirm')
    >>> request_line, request_line2 = [rl for rl in
    ...     sorted(request.lines, key=lambda x: x.production_order.id)]


Check exceed ul quantity with two request::

    >>> request2 = Request()
    >>> request2.producer = batch.producer
    >>> request_line3 = request2.lines.new()
    >>> request_line3.production_order = order
    >>> request2.click('confirm')# doctest: +ELLIPSIS
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Too many ULs for order "..." (to produce: ... ordered: ...) - 

    >>> request2.lines[0].production_order = None
    >>> request2.click('confirm')


Create another request and assign order with wizard::

    >>> request3 = Request()
    >>> request3.producer = batch.producer
    >>> request_line4 = request3.lines.new()
    >>> request_line4.production_order = order3
    >>> request_line4.ul_quantity = 2
    >>> request3.save()
    >>> len(request3.lines)
    1


Check duplicate production_order in request line::

    >>> request3.reload()
    >>> request4, = request3.duplicate()
    >>> len(request4.lines)
    1
    >>> bool(request4.lines[0].production_order)
    True
    >>> request3.lines[0].production_order == request4.lines[0].production_order
    True
    >>> request4.lines[0].production_order = None
    >>> request4.save()


Assign order with wizard in request3::

    >>> OrderData = Model.get('agro.harvest.request.assign_order.data')
    >>> with config.set_context(active_id=request3.id, active_model='agro.harvest.request'):
    ...     assign_order = Wizard('agro.harvest.request.assign_order', [request3])
    ...     to_assign, = OrderData.find([('production_order', '=', order)])
    >>> len(assign_order.form.production_orders)
    1
    >>> order_data, = assign_order.form.production_orders
    >>> order_data.production_order == order3
    True
    >>> order_data.request_line == request3.lines[0]
    True
    >>> order_data.code == order3.code
    True
    >>> order_data.reference == order3.reference
    True
    >>> order_data.party == order3.party
    True
    >>> order_data.shipment_address == order3.shipment_address
    True
    >>> order_data.crop_group == order3.crop_group
    True
    >>> order_data.crop == order3.crop
    True
    >>> order_data.attribute_category == order3.attribute_category
    True
    >>> order_data.attribute_caliber == order3.attribute_caliber
    True
    >>> order_data.attribute_confection == order3.attribute_confection
    True
    >>> order_data.ul_quantity == order3.ul_quantity
    True
    >>> order_data.ul_cases_quantity == order3.ul_cases_quantity
    True
    >>> order_data.quantity == order3.quantity
    True
    >>> order_data.uom == order3.uom
    True
    >>> order_data.ul_quantity_to_request
    2.0
    >>> order_data.ul_quantity_requested
    2.0
    >>> order_data.ul_quantity_to_request = 4

    >>> to_assign.ul_quantity_to_assign
    1.0
    >>> assign_order.form.production_orders.append(to_assign)
    >>> order_data2, = [o for o in assign_order.form.production_orders if o.production_order == order]
    >>> order_data2.ul_quantity_to_request
    1.0
    >>> order_data2.ul_quantity_requested
    3.0
    >>> assign_order.execute('add_')

    >>> len(request3.lines)
    2
    >>> rline1, = [l for l in request3.lines if l.production_order == order3]
    >>> rline2, = [l for l in request3.lines if l.production_order == order]
    >>> rline1.ul_quantity
    4.0
    >>> rline2.ul_quantity
    1.0
    >>> rline2.quantity_per_case == order.quantity_per_case
    True
    >>> request3.click('confirm')


Check order::

    >>> order.reload()
    >>> len(order.lines)
    1
    >>> order.lines[0].ul_quantity
    0.0


Edit request line::

    >>> request3.reload()
    >>> rline2, = [l for l in request3.lines if l.production_order == order]
    >>> rline2.ul_quantity = rline2.ul_quantity + 1
    >>> request3.save()


Check order::

    >>> order.reload()
    >>> order.lines[0].ul_quantity
    0.0


Create harvest line::

    >>> Harvest = Model.get('agro.harvest.line')
    >>> Year = Model.get('agro.year')
    >>> Plot = Model.get('agro.farm.plot')
    >>> plot1, = Plot.find([('name', '=', 'Plot 1')], limit=1)
    >>> year, = Year.find([], limit=1)
    >>> hline = Harvest()
    >>> hline.year = year
    >>> hline.batch = batch
    >>> hline.plot = plot1
    >>> hline.request_line = request_line
    >>> hline.close_request = True
    >>> hline.ul_quantity = Decimal(4)
    >>> hline.quantity_per_case = Decimal(12)
    >>> hline.save()
    >>> hline.click('wait')
    >>> hline.click('assign')
    >>> hline.click('run')
    >>> hline.click('do')

    >>> hline2 = Harvest()
    >>> hline2.year = year
    >>> hline2.batch = batch
    >>> hline2.plot = plot1
    >>> hline2.request_line = request_line2
    >>> hline2.close_request = True
    >>> hline2.ul_quantity = Decimal(3)
    >>> hline2.quantity_per_case = Decimal(12)
    >>> hline2.save()
    >>> hline2.click('wait')
    >>> hline2.click('assign')
    >>> hline2.click('run')
    >>> hline2.click('do')


Create receipt::

    >>> Location = Model.get('stock.location')
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> Receipt = Model.get('agro.goods.receipt')
    >>> receipt = Receipt()
    >>> rline = receipt.lines.new()
    >>> rline.harvest_line = hline
    >>> rline.location = storage_loc
    >>> rline2 = receipt.lines.new()
    >>> rline2.harvest_line = hline2
    >>> rline2.location = storage_loc
    >>> receipt.save()


Check sale unit loads of order::

    >>> len(order.sale_unit_loads)
    0
    >>> receipt.click('wait')
    >>> receipt.click('assign')
    >>> receipt.click('run')
    >>> time.sleep(1)
    >>> receipt.click('do')
    >>> order.reload()
    >>> len(order.sale_unit_loads)
    4
    >>> order2.reload()
    >>> len(order2.sale_unit_loads)
    3

Change one deny modified field in order with harvest request lines::

    >>> order.reload()
    >>> len(order.harvest_request_lines)
    2
    >>> order2, = order.duplicate()
    >>> len(order2.harvest_request_lines)
    0
    >>> order.click('draft')
    >>> order.state
    'draft'
    >>> order.attribute_category = template.attribute_combinations[2].category
    >>> order.save()
    Traceback (most recent call last):
    ...
    trytond.exceptions.UserError: Cannot modify following fields of Order "1" because it has harvest request lines:
      - Category - 
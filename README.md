datalife_agro_sale_board_harvest_request
========================================

The agro_sale_board_harvest_request module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-agro_sale_board_harvest_request/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-agro_sale_board_harvest_request)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT

# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import request
from . import receipt
from . import order


def register():
    Pool.register(
        request.Request,
        request.RequestLine,
        request.AssignOrderStart,
        request.AssignOrderData,
        receipt.Receipt,
        order.ProductionOrder,
        module='agro_sale_board_harvest_request', type_='model')
    Pool.register(
        request.AssignOrder,
        module='agro_sale_board_harvest_request', type_='wizard')
    Pool.register(
        module='agro_sale_board_harvest_request', type_='report')
    Pool.register(
        request.SaleBoardProduct,
        receipt.ReceiptProduct,
        order.ProductionOrderProduct,
        module='agro_sale_board_harvest_request', type_='model',
        depends=['agro_production_order_product'])
    Pool.register(
        request.LineBOM,
        module='agro_sale_board_harvest_request', type_='model',
        depends=['agro_harvest_request_bom'])
    Pool.register(
        request.LineExtraBOM,
        module='agro_sale_board_harvest_request', type_='model',
        depends=[
            'agro_harvest_request_bom',
            'agro_product_extra_bom'
        ])
    Pool.register(
        order.ProductionOrderAutoLine,
        module='agro_sale_board_harvest_request', type_='model',
        depends=['agro_production_order_autoline'])
    Pool.register(
        receipt.ReceiptHarvestless,
        module='agro_sale_board_harvest_request', type_='model',
        depends=['agro_harvest_request_purchase'])
